provider "google" {
  credentials = "${file("mygcp-creds.json")}"
  project     = "education-320707"
  region      = "us-west1"
}


resource "google_service_account" "default" {
  account_id   = "terraform"
  display_name = "Service Account"
}

// A single Compute Engine instance
resource "google_compute_instance" "default" {
 name         = "my-serv"
 machine_type = "e2-small"
 zone         = "us-west1-a"

 boot_disk {
   initialize_params {
     image = "debian-cloud/debian-9"
   }
 }

// Make sure flask is installed on all new instances for later steps
 metadata_startup_script = "sudo apt-get update; sudo apt-get install -yq build-essential python-pip rsync; pip install flask"

 network_interface {
   network = "default"

   access_config {
     // Include this section to give the VM an external ip address
   }
 }
}